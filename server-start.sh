#!/bin/sh
# Copyright (c) 2015 NikColonel
#
# System startup script for iDempiere server
#

#Server postfix used for startup multiply iDempiere server
SERVER_POSTFIX=8080
SERVER_NAME=iDempiereServer_$SERVER_POSTFIX

RED='\033[1;31m'
GREEN='\033[1;32m'
NORMAL='\033[0m'

start ()
{
	screen -dmS $SERVER_NAME sh idempiere-server.sh 
	echo "$SERVER_NAME running..."
}

stop ()
{
	screen -r $SERVER_NAME -X quit
	echo "$SERVER_NAME stoped..."	
}

status ()
{
	SERVER_STATUS=$(screen -ls | grep $SERVER_NAME)
		
	if [ "$SERVER_STATUS" ] 
	then
		echo "$SERVER_NAME ${GREEN}[Started]${NORMAL}"
	else
		echo "$SERVER_NAME ${RED}[Not Started]${NORMAL}"
	fi
}	

case "$1" in
	start)	start;;
	stop)	stop;;
	status)	status;;
	*)
		echo $"Usage: $0 {start|stop|status}"
		exit 1
esac
